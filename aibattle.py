#!/usr/bin/env python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository.GLib import timeout_add, source_remove
from gi.repository.Gdk import keyval_name

from colorsys import hsv_to_rgb
from itertools import permutations
from math import pi, e
from numpy import array, dot, append, zeros
from numpy.random import uniform, random, choice

from arena import Arena
from bot import Bot

def sigmoid(x):
	return 1/(1+e**(-x))

class Game:
	def __init__(self):
		self.arena = Arena()
		self.arena.connect('key-press-event', self.on_key_press)
		self.period = 15
		self.elapsed = 0
		self.game = 0
		self.generation = 1
		self.timer = timeout_add(self.period, self.on_timer)
		size = self.arena.size
		green = Bot(size/4, size/2, pi/2, self.arena)
		red = Bot(3*size/4, size/2, -pi/2, self.arena)
		self.bots = [green, red]
		self.num_ais = 6
		self.all_ais = [AI(self) for i in range(self.num_ais)]
		self.matches = permutations(self.all_ais, 2)
		print('Generation %d' % self.generation)
		self.new_game()

	def new_generation(self):
		self.game = 0
		self.generation += 1
		print('Generation %d' % self.generation)

		prob = zeros(self.num_ais)
		s = 0
		ais = self.all_ais
		best = ais[0]
		M = 0
		for i in range(self.num_ais):
			f = ais[i].fitness()
			prob[i] = f
			s += f
			if f > M:
				M = f
				best = ais[i]
		prob /= s

		new_ais = []
		for i in range(self.num_ais-1):
			a, b = choice(self.all_ais, 2, p=prob)
			new_ais.append(a.offspring(b))
		best.reset_stats()
		new_ais.append(best)

		self.all_ais = new_ais
		self.matches = permutations(self.all_ais, 2)

	def new_game(self):
		self.arena.draw_callbacks = [bot.draw for bot in self.bots]
		self.count = 0
		self.bullets = []
		try:
			self.ais = next(self.matches)
		except StopIteration:
			self.new_generation()
			self.ais = next(self.matches)

		self.game += 1
		print('Game %d' % self.game)

		for bot, ai in zip(self.bots, self.ais):
			bot.reset()
			ai.reset(bot)

		if self.period != 0:
			print('%d/%d/%d' % (self.ais[0].wins, self.ais[0].ties,
				self.ais[0].losses))
			print(self.ais[0].table1)
			print(self.ais[0].table2)
			print('%d/%d/%d' % (self.ais[1].wins, self.ais[1].ties,
				self.ais[1].losses))
			print(self.ais[1].table1)
			print(self.ais[1].table2)
			print(self.bots[0].life, self.bots[1].life, self.count)

	def check_game_over(self):
		game_over = False
		if self.count >= 5000:
			game_over = True
		elif any(not bot.alive() for bot in self.bots):
			game_over = True

		if not game_over:
			return

		if self.bots[0].life == self.bots[1].life:
			print('Tie')
			self.ais[0].tie()
			self.ais[1].tie()
		elif self.bots[0].life > self.bots[1].life:
			print('Left wins')
			self.ais[0].win()
			self.ais[1].lose(self.ais[0])
		else:
			print('Right wins')
			self.ais[1].win()
			self.ais[0].lose(self.ais[1])

		self.new_game()

	def on_timer(self, *args):
		next_bullets = []
		for bullet in self.bullets:
			bullet.move()
			alive = bullet.bounded()
			for bot in self.bots:
				if bot.hit(bullet):
					if self.period != 0:
						print(self.bots[0].life, self.bots[1].life,
							self.count)
					alive = False
					break
			if alive:
				next_bullets.append(bullet)
			else:
				bullet.owner.lost_bullet(bullet)
				self.arena.draw_callbacks.remove(bullet.draw)

		self.bullets = next_bullets

		for ai in self.ais:
			ai.next()

		self.count += 1
		self.elapsed += self.period

		self.check_game_over()
		if self.elapsed >= 15:
			self.arena.drawing_area.queue_draw()
			self.elapsed %= 15

		return True


	def on_key_press(self, widget, event):
		key = keyval_name(event.keyval)
		if 'Up' in key and self.period > 2:
			self.period -= 3
		elif 'Down' in key and self.period < 28:
			self.period += 3
		elif key == 'q':
			Gtk.main_quit()
			return False
		else:
			return True

		source_remove(self.timer)
		self.timer = timeout_add(self.period, self.on_timer)
		if self.period == 0:
			print('Redraw disabled')
		else:
			print('Frequency changed: %.4g Hz' % (1000/self.period))
		
		return True

class AI:
	def __init__(self, game):
		self.game = game
		self.color = hsv_to_rgb(random(), 1, random()/2+0.5)
		self.wins = 0
		self.ties = 0
		self.losses = 0
		self.inputs = 5
		self.hidden = 6
		self.outputs = 5
		self.table1 = uniform(-10, 10, (self.hidden, self.inputs+1))
		self.table2 = uniform(-10, 10, (self.outputs, self.hidden+1))

	def reset(self, bot):
		self.bot = bot
		self.state = 0
		self.bot.set_color(self.color)
		self.enemy = [b for b in self.game.bots if b != bot][0]

	def reset_stats(self):
		self.wins = 0
		self.ties = 0
		self.losses = 0

	def win(self):
		self.wins += 1

	def tie(self):
		self.ties += 1

	def lose(self, other):
		self.losses += 1

	def fitness(self):
		return max(0, 4*self.wins + self.ties - self.losses)

	def next(self):
		can_see_enemy = int(self.bot.can_see(self.enemy))
		can_shoot = int(self.bot.can_shoot(self.game.count))
		vision = self.bot.vision
		state = self.state
		const = 1

		can_see_bullets = 0
		for bullet in self.enemy.bullets:
			if self.bot.can_see(bullet):
				can_see_bullets = 1
				break

		inputs = array([can_see_enemy, can_see_bullets, can_shoot, vision, state, const])
		intermediate = sigmoid(dot(self.table1, inputs))
		intermediate = append(intermediate, const)
		outputs = sigmoid(dot(self.table2, intermediate))

		self.bot.rotate(outputs[0]*pi/16-pi/32)
		self.bot.move(outputs[1]*7)
		self.bot.change_vision(outputs[2]*pi/2)
		if can_shoot and outputs[3] > 0.5:
			bullet = self.bot.shoot(self.game.count)
			self.game.bullets.append(bullet)
			self.game.arena.draw_callbacks.append(bullet.draw)
		self.state = outputs[4]

	def offspring(self, other):
		o = AI(self.game)

		for j in range(self.hidden):
			for i in range(self.inputs+1):
				if random() < 0.8:
					o.table1[j][i] = self.table1[j][i]
				else:
					o.table1[j][i] = other.table1[j][i]

				if random() < 0.05:
					o.table1[j][i] = uniform(-10, 10)

		for j in range(self.outputs):
			for i in range(self.hidden+1):
				if random() < 0.8:
					o.table2[j][i] = self.table2[j][i]
				else:
					o.table2[j][i] = other.table2[j][i]

				if random() < 0.05:
					o.table2[j][i] = uniform(-10, 10)

		return o

def main():
	game = Game()
	Gtk.main()

if __name__ == '__main__': main()
