import gi
gi.require_version('Gtk', '3.0')
from gi.repository.Gtk import Window, DrawingArea, main_quit

from math import pi, cos, sin, hypot, acos, degrees

class Bot:
	def __init__(self, x, y, direction, arena):
		self.ini_x = x
		self.ini_y = y
		self.ini_direction = direction
		self.color = (0, 0, 0)
		self.arena = arena
		self.bullets = []

	def reset(self):
		self.last_shoot = -100
		self.x = self.ini_x
		self.y = self.ini_y
		self.r = 25
		self.direction = self.ini_direction
		self.vision = pi/8
		self.life = 10
		self.bullets = []
	
	def set_color(self, color):
		self.color = color

	def draw_vision(self, context):
		context.set_source_rgb(*self.color)
		context.set_line_width(2)
		a1 = self.direction + self.vision/2
		lx = self.x+250*cos(a1)
		ly = self.y-250*sin(a1)
		context.move_to(self.x, self.y)
		context.line_to(lx, ly)
		context.stroke()
		a2 = self.direction - self.vision/2
		lx = self.x+250*cos(a2)
		ly = self.y-250*sin(a2)
		context.move_to(self.x, self.y)
		context.line_to(lx, ly)
		context.stroke()

	def draw_bot(self, context):
		context.set_line_width(5)
		context.set_source_rgb(0, 0, 0)
		context.arc(self.x, self.y, self.r, 0, 2*pi)
		context.stroke_preserve()
		context.set_source_rgb(*self.color)
		context.fill()
		cx = self.x+(self.r-5)*cos(self.direction)
		cy = self.y-(self.r-5)*sin(self.direction)
		context.set_source_rgb(0, 0, 0)
		context.arc(cx, cy, 10, 0, 2*pi)
		context.stroke_preserve()
		context.set_source_rgb(*self.color)
		context.fill()

	def draw(self, widget, context):
		self.draw_vision(context)
		self.draw_bot(context)

	def move(self, steps):
		steps = max(steps, 0)
		steps = min(steps, 7)
		dx = steps*cos(self.direction)
		dy = steps*sin(self.direction)
		self.x += dx
		self.y -= dy
		self.x = max(self.x, 0)
		self.x = min(self.x, self.arena.size)
		self.y = max(self.y, 0)
		self.y = min(self.y, self.arena.size)

	def rotate(self, angle):
		angle = max(-pi/8, angle)
		angle = min(pi/8, angle)
		self.direction = (self.direction+angle) % (2*pi)

	def change_vision(self, angle):
		angle = max(angle, 5e-2)
		angle = min(angle, pi/2)
		self.vision = angle

	def can_see(self, obj):
		vx = obj.x-self.x
		vy = obj.y-self.y
		dist = hypot(vx, vy)
		bx = cos(self.direction)
		by = -sin(self.direction)
		dot = vx*bx + vy*by

		if dist == 0:
			return True

		x = dot/dist
		x = max(x, -1)
		x = min(x, 1)

		angle = acos(x)

		return angle <= self.vision/2

	def can_shoot(self, time_count):
		has_bullets = len(self.bullets) != 0
		passed_time = time_count-self.last_shoot > 40
		return passed_time and not has_bullets

	def shoot(self, time_count):
		px = self.x + (self.r+7)*cos(self.direction)
		py = self.y - (self.r+7)*sin(self.direction)

		bullet = Bullet(px, py, self.direction, self.arena, self)
		self.bullets.append(bullet)
		self.last_shoot = time_count
		return bullet

	def hit(self, obj):
		dist = hypot(obj.x-self.x, self.y-obj.y)
		r = dist <= self.r+obj.r

		if r:
			self.life -= 1

		return r

	def alive(self):
		return self.life > 0

	def lost_bullet(self, bullet):
		self.bullets.remove(bullet)

class Bullet:
	def __init__(self, x, y, direction, arena, owner):
		self.x = x
		self.y = y
		self.r = 5
		self.direction = direction
		self.arena = arena
		self.owner = owner

	def move(self):
		dx = 7*cos(self.direction)
		dy = 7*sin(self.direction)
		self.x += dx
		self.y -= dy

	def draw(self, widget, context):
		context.set_line_width(2)
		context.set_source_rgb(0, 0, 0)
		context.arc(self.x, self.y, self.r, 0, 2*pi)
		context.stroke_preserve()
		context.set_source_rgb(.2, .2, .2)
		context.fill()

	def bounded(self):
		x = self.x >= 0 and self.x < self.arena.size
		y = self.y >= 0 and self.y < self.arena.size
		return x and y
