import gi
gi.require_version('Gtk', '3.0')
from gi.repository.Gtk import Window, DrawingArea, main_quit, render_background

class Arena(Window):
	def __init__(self):
		super().__init__()
		self.connect('delete-event', main_quit)
		self.size = 950

		self.drawing_area = DrawingArea()
		self.drawing_area.connect('draw', self.on_draw)
		self.drawing_area.set_size_request(self.size, self.size)
		self.add(self.drawing_area)
		self.draw_callbacks = []
		self.show_all()

	def on_draw(self, widget, context):
		context.set_source_rgb(0, 0, 0)
		context.set_line_width(1)
		context.move_to(self.size/2, 0)
		context.line_to(self.size/2, self.size)
		context.stroke()

		for cb in self.draw_callbacks:
			cb(widget, context)

		return True
